/*    
 * @licstart  The following is the entire license notice for the 
 * JavaScript code in this page.
 *
 * Copyright (c) 2022-2023 PortaStream Team.
 *
 * The JavaScript code in this page is free software: you can
 * redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License (GNU AGPL) as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.  The code is distributed WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU AGPL for more details.
 *
 * As additional permission under GNU AGPL version 3 section 7, you
 * may distribute non-source (e.g., minimized or compacted) forms of
 * that code without the copy of the GNU AGPL normally required by
 * section 4, provided you include this license notice and a URL
 * through which recipients can access the Corresponding Source.   
 *
 * @licend  The above is the entire license notice
 * for the JavaScript code in this page.
 */

let queue;
let audio_player;
let dom_cover;
let dom_title;
let dom_artist;
let dom_play;
let dom_details;
let dom_loop;
let dom_mute;
let dom_volume;
let dom_timestamp;
let dom_progbar;
let progbar_occupied = false;

function store_queue() {
	localStorage.setItem('player.queue', JSON.stringify(queue));
}

function player_init() {
	queue = JSON.parse(localStorage.getItem('player.queue')) || {
		'cursor': 0,
		'queue': []
	};

	audio_player = document.createElement('audio');
	audio_player.onerror = function() {
		popup('Error', 'Failed to load media.');
	}
	/* audio_player.onended = onEnded; */

	dom_cover = document.getElementById('cover');
	dom_title = document.getElementById('title');
	dom_artist = document.getElementById('artist');
	dom_play = document.getElementById('play');
	dom_details = document.getElementById('details');
	dom_loop = document.getElementById('loop');
	dom_mute = document.getElementById('mute');
	dom_volume = document.getElementById('volume');
	dom_timestamp = document.getElementById('timestamp');
	dom_progbar = document.getElementById('progbar');
	setInterval(refresh_state, 200)
}

/* Note: HTMLMediaElement.videoTracks returns video tracks of a file; might be of use in the future? */

function refresh_state() {
	if(audio_player.paused) dom_play.classList = 'icon-play';
	else dom_play.classList = 'icon-pause';
	dom_timestamp.innerText = `${format_duration(audio_player.currentTime)} / ${format_duration(audio_player.duration)}`;
	if(!progbar_occupied) dom_progbar.value = audio_player.currentTime / audio_player.duration * dom_progbar.max;
}

function play_track(adapter_index, id) {
	request_endpoint(adapter_index, 'metadata?id=' + id,
		function() {
			dom_timestamp.innerText = '00:00 / 00:00';
			dom_cover.src = `${adapters[adapter_index].address}/fetch_cover?id=${id}`;
			dom_cover.style.opacity = 1;
			dom_title.innerText = this.response.title;
			dom_artist.innerText = this.response.artist;
			dom_details.setAttribute('onclick',
				`show_track_details(${adapter_index}, "${id}")`
			);
			navigator.mediaSession.metadata = new MediaMetadata({
				title: this.response.title,
				artist: this.response.artist,
				album: this.response.album,
				artwork: [{src: `${adapters[adapter_index].address}/fetch_cover?id=${id}`}]
			});
			audio_player.src = `${adapters[adapter_index].address}/fetch?id=${id}`;
			audio_player.play();
		},
		function() {
			popup('Error', 'Failed to fetch metadata.');
		}
	);
}

function toggle_playback() {
	if(audio_player.paused) audio_player.play();
	else audio_player.pause();
	refresh_state();
}

function relocate(percentage) {
	audio_player.currentTime = percentage / progbar.max * audio_player.duration;
	refresh_state();
}

function show_track_details(adapter_index, id) {
	request_endpoint(adapter_index, 'metadata?id=' + id,
		function() {
			popup(
				'Details',
`Title: ${this.response.title}
Artist: ${this.response.artist}
Album: ${this.response.album}
Genre: ${this.response.genre}
Duration: ${format_duration(this.response.duration)}
Date: ${format_julian_day(this.response.date)}

File size: ${format_size(this['response']['file-size'])}
Modified: ${format_time(this.response.mtime)}
Bitrate: ${this.response.bitrate / 1000} kbps
Media type: ${this['response']['media-type']}

Adapter: ${adapters[adapter_index].name}
ID: ${id}`
			);
		},
		function() {
			popup('Error', 'Failed to fetch metadata.');
		}
	);
}

function toggle_loop() {
	if(audio_player.loop) loop.classList = 'icon-loop';
	else loop.classList = 'icon-loop activated';
	audio_player.loop = !audio_player.loop;
}

function toggle_mute() {
	if(audio_player.volume == 0) {
		set_volume(100);
	}
	else {
		set_volume(0);
	}
}

function set_volume(percentage) {
	if(percentage == 0) mute.classList = 'icon-mute';
	else mute.classList = 'icon-volume';
	audio_player.volume = percentage / 100;
	dom_volume.value = percentage;
}
