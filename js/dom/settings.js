/*    
 * @licstart  The following is the entire license notice for the 
 * JavaScript code in this page.
 *
 * Copyright (c) 2022-2023 PortaStream Team.
 *
 * The JavaScript code in this page is free software: you can
 * redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License (GNU AGPL) as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.  The code is distributed WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU AGPL for more details.
 *
 * As additional permission under GNU AGPL version 3 section 7, you
 * may distribute non-source (e.g., minimized or compacted) forms of
 * that code without the copy of the GNU AGPL normally required by
 * section 4, provided you include this license notice and a URL
 * through which recipients can access the Corresponding Source.   
 *
 * @licend  The above is the entire license notice
 * for the JavaScript code in this page.
 */

let adapters;
let dom_adapter_list;
let dom_adapter_list_body;

function settings_init() {
	adapters = JSON.parse(localStorage.getItem('settings.adapters')) || [];
	dom_adapter_list = document.getElementById('adapter-list');
	dom_adapter_list_body = dom_adapter_list.getElementsByTagName('tbody')[0];
	dom_refresh_adapter_count();
	for(let i = 0; i < adapters.length; i ++) {
		dom_add_adapter(adapters[i].name, adapters[i].address);
	}
	dom_refresh_all_adapters();
}

function store_adapters() {
	localStorage.setItem('settings.adapters', JSON.stringify(adapters));
}

function dom_get_adapter_index(button) {
	return button.parentNode.parentNode.rowIndex - 1;
}

function dom_refresh_all_adapters() {
	for(let i = 0; i < adapters.length; i ++) {
		dom_refresh_adapter(i);
	}
}

function dom_refresh_adapter_count() {
	document.getElementById('adapter-count').innerText = adapters.length;
	if(adapters.length == 0) dom_adapter_list.style.display = 'none';
	else dom_adapter_list.style.display = 'table';
}

function dom_add_adapter(name, address) {
	let entry = document.getElementById('adapter-entry');
	let fields = entry.content.querySelectorAll('span');
	fields[0].innerText = name;
	fields[1].innerText = address;
	dom_adapter_list_body.appendChild(document.importNode(entry.content, true));
}

function dom_refresh_adapter(index) {
	let dom_status = dom_adapter_list_body.children[index].getElementsByTagName('span')[2];
	dom_status.innerText = 'Loading...';
	request_endpoint(index, 'desc',
		function() {
			if(!this.response || !this.response.spec_version) {
				dom_status.innerText = 'Unexpected response';
				return;
			}
			if(this.response.spec_version != '0.0.1') {
				dom_status.innerText = 'Incompatible';
				return;
			}
			dom_status.innerText = 'OK';
		},
		function() {
			dom_status.innerText = 'Failed';
		}
	);
}

function dom_show_adapter_details(index) {
	request_endpoint(index, 'desc',
		function() {
			if(!this.response || !this.response.spec_version) {
				popup('Error', 'Unexpected response. Is it an adapter?');
				return;
			}
			popup(
				'Details',
`Name: ${this.response.name}
Author: ${this.response.author}
Website: ${this.response.website}
License: ${this.response.license}
Spec version: ${this.response.spec_version}`
			);
		},
		function() {
			popup('Error', 'Failed to fetch adapter description.');
		}
	);
}

function dom_edit_adapter(index) {
	let row = dom_adapter_list_body.children[index];
	let fields = row.getElementsByTagName('input');
	fields[0].value = adapters[index].name;
	fields[1].value = adapters[index].address;
	row.classList = ['editing'];
	fields[1].focus();
}

function dom_save_adapter_changes(index) {
	let row = dom_adapter_list_body.children[index];
	let fields = row.getElementsByTagName('input');
	let spans = row.getElementsByTagName('span');
	spans[0].innerText = adapters[index].name = fields[0].value;
	spans[1].innerText = adapters[index].address = fields[1].value;
	store_adapters();
	row.classList = [];
	spans[2].innerText = '?';
	dom_refresh_adapter(index);
}

function dom_discard_adapter_changes(index) {
	dom_adapter_list_body.children[index].classList = [];
}

function dom_remove_adapter(index) {
	dom_adapter_list_body.children[index].remove();
	adapters.splice(index, 1);
	store_adapters();
	dom_refresh_adapter_count();
}

function dom_create_adapter(name, address) {
	adapters.push({
		name: name,
		address: address
	});
	store_adapters();
	dom_refresh_adapter_count();
	dom_add_adapter(name, address);
	dom_edit_adapter(adapters.length - 1);
}
