/*    
 * @licstart  The following is the entire license notice for the 
 * JavaScript code in this page.
 *
 * Copyright (c) 2022-2023 PortaStream Team.
 *
 * The JavaScript code in this page is free software: you can
 * redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License (GNU AGPL) as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.  The code is distributed WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU AGPL for more details.
 *
 * As additional permission under GNU AGPL version 3 section 7, you
 * may distribute non-source (e.g., minimized or compacted) forms of
 * that code without the copy of the GNU AGPL normally required by
 * section 4, provided you include this license notice and a URL
 * through which recipients can access the Corresponding Source.   
 *
 * @licend  The above is the entire license notice
 * for the JavaScript code in this page.
 */

let dom_search_box;
let dom_indicator;
let dom_search_result;
let dom_search_result_body;
let matches;
let loaded_adapters;
let failed_adapters;

function discover_init() {
	dom_search_box = document.getElementById('search-box');
	dom_indicator = document.getElementById('search-indicator');
	dom_search_result = document.getElementById('search-result');
	dom_search_result_body = dom_search_result.getElementsByTagName('tbody')[0];
	dom_search_box.onkeydown = function(e) {
		if(e.code == 'Enter' || e.code == 'NumpadEnter') dom_search();
	}
}

function refresh_search_result_count() {
	dom_indicator.innerText =`${matches} results (from ${adapters.length} adapters: ${loaded_adapters} done, ${failed_adapters} failed)`;
}

function dom_search() {
	if(dom_search_box.value.length == 0) {
		dom_search_result.style.display = 'none';
		dom_indicator.innerText = '';
		return;
	}

	matches = 0;
	loaded_adapters = 0;
	failed_adapters = 0;
	refresh_search_result_count();

	dom_search_result_body.innerHTML = '';
	dom_search_result.style.display = 'table';

	for(let index = 0; index < adapters.length; index ++) 
	request_endpoint(index, dom_search_box.value == '*' ? 'list' : 'search?kwards=' + encodeURIComponent(dom_search_box.value),
		function() {
			/* `/list` or `/search` return a list of ids for further meta query */
			request_endpoint(index, 'multiple_metadata',
				function() {
					for(j in this.response) {
						let entry = document.getElementById('search-result-entry');
						let columns = entry.content.querySelectorAll('span');
						columns[0].innerText = this.response[j].title;
						columns[1].innerText = this.response[j].artist;
						columns[2].innerText = this.response[j].album;
						columns[3].innerText = format_duration(this.response[j].duration);
						let buttons = entry.content.querySelectorAll('button');
						buttons[0].setAttribute('onclick',
							`play_track(${index}, "${this.response[j].location}"); window.location.hash = '#/player';`
						);
						buttons[1].setAttribute('onclick',
							`popup('Info', 'This feature has not been implemented yet.')`
						);
						buttons[2].setAttribute('onclick',
							`show_track_details(${index}, "${this.response[j].location}")`
						);
						dom_search_result_body.appendChild(document.importNode(entry.content, true));
						matches ++;
					}
					loaded_adapters ++;
					refresh_search_result_count();
				},
				function() {
					failed_adapters ++;
					refresh_search_result_count();
				},
				JSON.stringify(this.response)
			);
		},
		function() {
			failed_adapters ++;
			refresh_search_result_count();
		}
	);
}
