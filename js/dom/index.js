/*
 * @licstart  The following is the entire license notice for the 
 * JavaScript code in this page.
 *
 * Copyright (c) 2022-2024 PortaStream Team.
 *
 * The JavaScript code in this page is free software: you can
 * redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License (GNU AGPL) as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.  The code is distributed WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU AGPL for more details.
 *
 * As additional permission under GNU AGPL version 3 section 7, you
 * may distribute non-source (e.g., minimized or compacted) forms of
 * that code without the copy of the GNU AGPL normally required by
 * section 4, provided you include this license notice and a URL
 * through which recipients can access the Corresponding Source.   
 *
 * @licend  The above is the entire license notice
 * for the JavaScript code in this page.
 */

let levels;
let dom_tabs;

window.onload = function() {
	dom_tabs = document.getElementById('tabs').children;
	window.onhashchange(); 
	popup(
		'Note',
`PortaStream Web is being refactored and
may not function at all at the moment.`
	);
	/*
	settings_init();
	player_init();
	discover_init();
	playlists_init() */
}

window.onhashchange = function() {
	levels = window.location.hash.substring(2).split('/');
	if (!levels[0]) levels = ['player'];
	for (let i = 0; i < dom_tabs.length; i ++) {
		let tab_name = dom_tabs[i].getAttribute('href').substring(2);
		if (tab_name == levels[0]) {
			dom_tabs[i].classList = 'selected';
			document.getElementById('content').src = levels[0] + '.html';
		}
		else {
			dom_tabs[i].classList = '';
		}
	}	
}

function popup(title, body) {
	document.getElementById('popup-title').innerText = title;
	document.getElementById('popup-body').innerText = body;
	let popup = document.getElementById('popup');
	popup.style.display = 'inline';
	void popup.offsetHeight;
	popup.style.opacity = 1;
}

function close_popup() {
	document.getElementById('popup').style.opacity = 0;
	document.getElementById('popup').style.display = 'none';
}
