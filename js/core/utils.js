/*
 * @licstart  The following is the entire license notice for the 
 * JavaScript code in this page.
 *
 * Copyright (c) 2022-2024 PortaStream Team.
 *
 * The JavaScript code in this page is free software: you can
 * redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License (GNU AGPL) as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.  The code is distributed WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU AGPL for more details.
 *
 * As additional permission under GNU AGPL version 3 section 7, you
 * may distribute non-source (e.g., minimized or compacted) forms of
 * that code without the copy of the GNU AGPL normally required by
 * section 4, provided you include this license notice and a URL
 * through which recipients can access the Corresponding Source.   
 *
 * @licend  The above is the entire license notice
 * for the JavaScript code in this page.
 */

function format_duration(timestamp) {
	if(!timestamp) return '00:00';

	let result = '';
	if(timestamp >= 3600) {
		result += String(Math.floor(timestamp / 3600)).padStart(2, '0') + ':';
		timestamp %= 3600;
	}
	result += String(Math.floor(timestamp / 60)).padStart(2, '0') + ':';
	result += String(Math.floor(timestamp % 60)).padStart(2, '0');
	return result;
}

function format_julian_day(day) {
	if(!day) return '?';

	let date = new Date(day * 86400000 - 62135596800000);
	return (
		String(date.getFullYear()) + '/' +
		String(date.getMonth() + 1).padStart(2, '0') + '/' +
		String(date.getDate()).padStart(2, '0')
	);
}

function format_time(timestamp) {
	if(!timestamp) return '?';

	let date = new Date(timestamp * 1000);
	return (
		String(date.getFullYear()) + '-' +
		String(date.getMonth() + 1).padStart(2, '0') + '-' +
		String(date.getDate()).padStart(2, '0') + ' ' + 
		String(date.getHours()).padStart(2, '0') + ':' + 
		String(date.getMinutes()).padStart(2, '0') + ':' + 
		String(date.getSeconds()).padStart(2, '0')
	);
}

function format_size(byte) {
	let pos = 0;
	while(byte >= 1024) {
		byte /= 1024;
		pos ++;
	}
	return Math.round(byte, 2) + ' ' + ['B', 'KB', 'MB', 'GB', 'TB', 'PB'][pos];
}

function request_endpoint(adapter_index, endpoint, success, error, body) {
	let xhr = new XMLHttpRequest();
	let url = adapters[adapter_index].address + '/' + endpoint;
	xhr.responseType = 'json';
	xhr.onload = success;
	xhr.onerror = error;

	if(body === undefined) {
		xhr.open('GET', url, true);
		xhr.send();
	}
	else {
		xhr.open('POST', url, true);
		xhr.send(body);
	}
}
